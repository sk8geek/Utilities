/* 
 * @(#) IntegerBackedListPreference.java		0.1		2014/05/14
 * 
 * Copyright 2014 Steven J Lilley
 *
 * A timer preference that presents NumberPickers to allow setting of seconds
 * and tenths of a second. The value persists as a 'long' value.
 *
 * IntegerBackedListPreference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.util.Log;

public class IntegerBackedListPreference extends ListPreference {
	
	static String TAG = "IBLP";
	static final int DEFAULT_VALUE = 0;
	int defaultValue = DEFAULT_VALUE;
	private int index = DEFAULT_VALUE;

    public IntegerBackedListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setPositiveButtonText(android.R.string.ok);
        //setNegativeButtonText(android.R.string.cancel);
        //setDialogIcon(null);
    }
    
	@Override
    protected void onDialogClosed(boolean save) {
		if (save) {
			super.onDialogClosed(save);
			Log.d(TAG, "getValue()=" + getValue());
			index = findIndexOfValue(getValue());
			persistInt(index);
		}
		//setSummary(getEntries()[index]);
	}

	@Override
	protected Object onGetDefaultValue(TypedArray array, int idx) {
		index = array.getInt(idx, DEFAULT_VALUE);
		return index;
	}
	
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState myState = (SavedState)state;
		super.onRestoreInstanceState(myState.getSuperState());
		index = myState.value;
		setValueIndex(index);
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			return superState;
		}
		final SavedState myState = new SavedState(superState);
		myState.value = findIndexOfValue(getValue());
		return myState;
	}

	@Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
		if (restorePersistedValue) {
			index = this.getPersistedInt(DEFAULT_VALUE);
		} else {
			index = (Integer)defaultValue;
			persistInt(index);
		}
		// FIXME: setSummary(getEntries()[index]);
		// setValueIndex(index);
		// TODO: above causes a null pointer exception
    }

	private static class SavedState extends BaseSavedState {
		int value;
	
		public SavedState(Parcelable superState) {
			super(superState);
		}
	
		public SavedState(Parcel source) {
			super(source);
			value = source.readInt();
		}
	
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(value);
		}
	
		public static final Parcelable.Creator<SavedState> CREATOR =
				new Parcelable.Creator<SavedState>() {
	
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}
	
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}	
	
}
