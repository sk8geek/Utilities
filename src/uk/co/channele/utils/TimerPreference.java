/* 
 * @(#) TimerPreference.java		0.1		2014/05/14
 * 
 * Copyright 2014 Steven J Lilley
 *
 * A timer preference that presents NumberPickers to allow setting of seconds
 * and tenths of a second. The value persists as a 'long' value.
 *
 * TimerPreference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

public class TimerPreference extends DialogPreference {
	
	static String TAG = "TP";
	static final long DEFAULT_VALUE = 1000l;
	long defaultValue = DEFAULT_VALUE;
	private long duration = 0l;
	private int seconds = 0;
	private int tenths = 0;
	private NumberPicker secondPicker;
	private NumberPicker tenthPicker;

    public TimerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(R.layout.timer_preference_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
        //setDialogIcon(null);
    }
    
    @Override
    protected View onCreateDialogView() {
    	View view = super.onCreateDialogView();
        secondPicker = (NumberPicker)view.findViewById(R.id.timerPreferenceSeconds);
		secondPicker.setMinValue(0);
		secondPicker.setMaxValue(9);
		secondPicker.setValue(seconds);
		tenthPicker = (NumberPicker)view.findViewById(R.id.timerPreferenceTenths);
		tenthPicker.setMinValue(0);
		tenthPicker.setMaxValue(9);
		tenthPicker.setValue(tenths);
		return view;
	}	
    
	@Override
    protected void onDialogClosed(boolean save) {
		if (save) {
			seconds = secondPicker.getValue();
			tenths = tenthPicker.getValue();
			duration =  seconds * 1000l + tenths * 100l;
			persistLong(duration);
			setSummary(seconds + "." + tenths + " seconds");
		}
	}

	@Override
	protected Object onGetDefaultValue(TypedArray array, int index) {
		int defaultAsInt = 0;
		defaultAsInt = array.getInt(index, (int)DEFAULT_VALUE);
		return (long)defaultAsInt;
	}
	
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState myState = (SavedState)state;
		super.onRestoreInstanceState(myState.getSuperState());
		duration = myState.value;
		seconds = (int)(duration / 1000);
		tenths = (int)(duration / 100 - (seconds * 10));
		secondPicker.setValue(seconds);
		tenthPicker.setValue(tenths);
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			return superState;
		}
		final SavedState myState = new SavedState(superState);
		myState.value = duration;
		return myState;
	}

	@Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
		if (restorePersistedValue) {
			duration = this.getPersistedLong(DEFAULT_VALUE);
		} else {
			duration = (Long)defaultValue;
			persistLong(duration);
		}
		seconds = (int)(duration / 1000);
		tenths = (int)(duration / 100 - (seconds * 10));
        setSummary(seconds + "." + tenths + " seconds");
		//secondPicker.setValue(seconds);
		//tenthPicker.setValue(tenths);
    }

	private static class SavedState extends BaseSavedState {
		long value;
	
		public SavedState(Parcelable superState) {
			super(superState);
		}
	
		public SavedState(Parcel source) {
			super(source);
			value = source.readLong();
		}
	
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeLong(value);
		}
	
		public static final Parcelable.Creator<SavedState> CREATOR =
				new Parcelable.Creator<SavedState>() {
	
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}
	
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}	
	
}
