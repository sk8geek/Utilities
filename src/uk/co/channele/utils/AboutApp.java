/* 
 * @(#) AboutApp.java	1.0 	2013/09/05
 * 
 * Copyright 2013 Steven J Lilley
 *
 * It provides a simple dialog fragment giving app/licence details.
 *
 * This file is part of JamTimer. 
 *
 * JamTimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

public class AboutApp extends DialogFragment {
	
	public static AboutApp newInstance(int id) {
		AboutApp picker = new AboutApp();
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        picker.setArguments(bundle);
        return picker;
    }
    
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		Dialog aboutapp;
		View view = inflater.inflate(R.layout.aboutapp, null);
		builder.setView(view);
		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				AboutApp.this.getDialog().cancel();
			}
		});      
		aboutapp = builder.create();
		return aboutapp;
	}
	
}

